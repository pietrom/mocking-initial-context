package org.amicofragile.test.context.foobar;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.amicofragile.test.context.hello.Hello;

public class FooBar {
	private Hello hello;
	
	public FooBar() throws NamingException {
		hello = (Hello) new InitialContext().lookup("services/Hello");
	}
	
	public void sayHelloTo(String to) {
		hello.sayHelloTo(to);
	}
}
