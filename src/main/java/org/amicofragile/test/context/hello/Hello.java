package org.amicofragile.test.context.hello;

public interface Hello {
	public abstract String sayHelloTo(String to);
}
