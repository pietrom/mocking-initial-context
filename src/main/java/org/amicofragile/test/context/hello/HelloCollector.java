package org.amicofragile.test.context.hello;

public class HelloCollector implements Hello {
	private String lastTarget = null;
	
	public String getLastTarget() {
		return lastTarget;
	}

	public String sayHelloTo(String to) {
		lastTarget = to;
		return to;
	}
}
