package org.amicofragile.test.context;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

public class MockedInitialContextFactory implements InitialContextFactory {
	private static Context context;
	
	static {
		context = new ContextAdapter() {
			private final Map<String, Object> ctx = new HashMap<String, Object>();
			
			@Override
			public Object lookup(String name) throws NamingException {
				return ctx.get(name);
			}
			
			@Override
			public void bind(String name, Object obj) throws NamingException {
				ctx.put(name, obj);
			}
		};
	}
	
	public static void bind(String name, Object object) throws NamingException {
		context.bind(name, object);
	}

	public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
		return context;
	}
}
