package org.amicofragile.test.context;

import static org.junit.Assert.*;

import javax.naming.Context;

import org.amicofragile.test.context.foobar.FooBar;
import org.amicofragile.test.context.hello.HelloCollector;
import org.junit.Test;

public class FooBarTest {
	@Test
	public void usesMockedInitialContext() throws Exception {
		HelloCollector collector = new HelloCollector();
		MockedInitialContextFactory.bind("services/Hello", collector);
		System.setProperty(Context.INITIAL_CONTEXT_FACTORY, MockedInitialContextFactory.class.getName());
		FooBar fb = new FooBar();
		fb.sayHelloTo("Pietro");
		
		assertEquals("Pietro", collector.getLastTarget());
	}
}
